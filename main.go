package main

import (
	"fmt"

	"github.com/colinmarc/hdfs"
)

func main() {
	client, err := hdfs.New("hdfs-namenode:8020")
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	file, err := client.Create("/test.txt")
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	file.Close()
}
